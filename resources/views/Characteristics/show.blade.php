@include('admin.navigation')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>Détail de la caractéristique</h1>
                <p class="col-6">Titre {{$characteristics->title}}</p>
            <a href="{{route('characteristicsEdit', $parameters = ['characteristic'=>$characteristics])}}" class='btn btn-info'>Modifier</a>
            <a href="{{route('characteristicsIndex')}}" class='btn btn-info'>Liste des caractéristiques </a>
        </div>
    </div>
</div>
</body>
</html>
