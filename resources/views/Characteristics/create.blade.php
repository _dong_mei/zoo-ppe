@include('admin.navigation')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>Création d'une caractéristique</h1>
            <form action="" method="POST">
                @csrf
                <label for="title">Titre de la caractéristique</label>
                <input type="text" name="title" required="required">
                <input type="submit" class="button" value="Enregistrer">
            </form>
        </div>
    </div>
</div>
</body>
</html>
