@include('admin.navigation')
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="flex-row">
                <h1>Liste des caractéristiques</h1>
                <a class="button" href="{{ route('characteristicsCreate') }}">Créer une nouvelle caractéristique</a>
            </div>
            <table class="table" style="width: 100%">
                <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Titre</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($characteristics as $characteristic)
                    <tr>
                        <td>{{$characteristic->id}}</td>
                        <td>{{$characteristic->title}}</td>
                        <td>
                            <a href="{{route('characteristicsShow', $parameters = ['id'=>$characteristic->id])}}" class='btn btn-info'>Voir</a>
                            <a href="{{route('characteristicsEdit', $parameters = ['characteristic'=>$characteristic])}}" class='btn btn-info'>Modifier</a>
                            <form method="POST" action="/admin/characteristics/{{$characteristic->id}}/destroy" >
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="submit" value="supprimer"/>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
</div>
</body>
</html>

