@include('admin.navigation')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>Modification d'une caractéristique</h1>
            <form method="post" action="{{route('characteristicsUpdate', ['characteristic'=> $characteristic->id])}}">
                @csrf
                <input type="hidden" name="_method" value="put">
                    <label for="title">Titre</label>
                    <input type="text" value="{{$characteristic->title}}" name="title" required="required">
                <button type="submit" class="button">Enregistrer</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>
