@include('admin.navigation')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>Création d'un animal</h1>
            @if($errors->has('name'))
                <p>Le champ « Nom » a une erreur</p>
            @endif
            @if($errors->has('type_id'))
                <p>Le champ « Espèce » a une erreur</p>
            @endif
            @if($errors->has('characteristics'))
                <p>Le champ « Caractéristiques » a une erreur</p>
            @endif
            <form action="" method="POST">
                @csrf
                <div class="split">
                    <div>
                        <label for="name">Nom de l'animal</label>
                        <input type="text" name="name">
                    </div>
                    <div>
                        <label for="gestation">Reproduction</label>
                        <input type="text" name="gestation">
                    </div>
                </div>
                <div class="split">
                    <div>
                        <label for="weight">Poids</label>
                        <input type="text" name="weight">
                    </div>
                    <div>
                        <label for="height">Taille</label>
                        <input type="text" name="height">
                    </div>
                </div>
                <div class="split">
                    <div>
                        <label for="environment">Milieu</label>
                        <input type="text" name="environment">
                    </div>
                    <div>
                        <label for="diet">Alimentation</label>
                        <input type="text" name="diet">
                    </div>
                </div>
                <div class="split">
                    <div>
                        <label for="lifetime">Espérence de vie</label>
                        <input type="text" name="lifetime">
                    </div>
                    <div>
                        <label for="type_id">Espèce</label>
                        <select name="type_id">
                            @foreach($types as $type)
                                <option name="type_id" value="{{$type->id}}">{{$type->title}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <label for="characteristics">Caractéristiques</label>
                <select name="characteristics[]" multiple="multiple">
                    @foreach($characteristics as $characteristic)
                        <option value="{{$characteristic->id}}">{{$characteristic->title}}</option>
                    @endforeach
                </select>
                <input type="submit" class="button" value="Enregistrer">
            </form>
        </div>
    </div>
</div>
</body>
</html>
