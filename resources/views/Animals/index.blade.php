@include('admin.navigation')
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="flex-row">
                <h1>Liste des animaux</h1>
                <a class="button" href="{{ route('animalsCreate') }}">Ajouter un animal</a>
            </div>
            <table class="table" style="width: 100%">
                <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Nom</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($animals as $animal)
                    <tr>
                        <td>{{$animal->id}}</td>
                        <td>{{$animal->name}}</td>
                        <td>
                            <a href="{{route('animalsShow', $parameters = ['id'=>$animal->id])}}" class='btn btn-info'>Voir</a>
                            <a href="{{route('animalsEdit', $parameters = ['animal'=>$animal])}}" class='btn btn-info'>Modifier</a>
                            <form method="POST" action="/admin/animals/{{$animal->id}}/destroy" >
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="submit" value="supprimer"/>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
</div>
</body>
</html>
