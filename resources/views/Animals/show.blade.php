@include('admin.navigation')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>Détail d'un animal</h1>
                <p>Id: {{$animals->id}}</p>
                <p class="col-6">Titre: {{$animals->name}}</p>
                <p class="col-6">Reproduction: {{$animals->gestation}}</p>
                <p class="col-6">Poids: {{$animals->weight}}</p>
                <p class="col-6">Taille: {{$animals->heigth}}</p>
                <p class="col-6">Lieu de vie: {{$animals->environment}}</p>
                <p class="col-6">Alimentation: {{$animals->diet}}</p>
                <p class="col-6">Durée de vie: {{$animals->lifetime}}</p>
                <p>Caractéristiques:</p>
                <ul>
                    @foreach($animals->characteristics as $characteristic)
                        <li>{{ $characteristic->title }}</li>
                    @endforeach
                </ul>
                <p>Espèce: {{$type->title}}</p>

            <a href="{{route('animalsEdit', $parameters = ['animal'=>$animals])}}" class='btn btn-info'>Modifier</a>
            <a href="{{route('animalsIndex')}}" class='btn btn-info'>Liste des animaux </a>
        </div>
    </div>
</div>
</body>
</html>
