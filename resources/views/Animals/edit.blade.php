@include('admin.navigation')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>Modification d'un animal</h1>
            <form action="{{route('animalsUpdate', ['animal'=> $animal->id])}}" method="POST">
                @csrf
                <input type="hidden" name="_method" value="put">
                <div class="split">
                    <div>
                        <label for="name">Nom de l'animal</label>
                        <input type="text" name="name" value="{{$animal->name}}" required="required">
                    </div>
                    <div>
                        <label for="gestation">Reproduction</label>
                        <input type="text" name="gestation" value="{{$animal->gestation}}">
                    </div>
                </div>
                <div class="split">
                    <div>
                        <label for="weight">Poids</label>
                        <input type="text" name="weight" value="{{$animal->weight}}">
                    </div>
                    <div>
                        <label for="height">Taille</label>
                        <input type="text" name="height" value="{{$animal->heigth}}">
                    </div>
                </div>
                <div class="split">
                    <div>
                        <label for="environment">Milieu</label>
                        <input type="text" name="environment" value="{{$animal->environment}}">
                    </div>
                    <div>
                        <label for="diet">Alimentation</label>
                        <input type="text" name="diet" value="{{$animal->diet}}">
                    </div>
                </div>
                <div class="split">
                    <div>
                        <label for="lifetime">Espérence de vie</label>
                        <input type="text" name="lifetime" value="{{$animal->lifetime}}">
                    </div>
                    <div>
                        <label for="type_id">Espèce</label>
                        <select name="type_id" required="required">
                            @foreach($types as $type)

                                <option name="type_id" value="{{$type->id}}" @if($type->id == $animal->type_id) selected @endif>{{$type->title}}</option>

                            @endforeach
                        </select>
                    </div>
                </div>
                <label for="characteristics">Caractéristiques</label>
                <select name="characteristics[]" multiple="multiple" required="required">
                    @foreach($characteristics as $characteristic)
                        <option value="{{$characteristic->id}}"
                                @foreach($animal->characteristics as $defaultcharacteristic)
                                @if($characteristic->id == $defaultcharacteristic->id) selected @endif
                            @endforeach
                        >{{$characteristic->title}}</option>
                    @endforeach
                </select>
                <input type="submit" class="button" value="Enregistrer">
            </form>
        </div>
    </div>
</div>
</body>
</html>

