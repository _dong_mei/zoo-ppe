<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <title>ZOO</title>
</head>
<body>
<nav class="homepage-navbar">
    <div class="logo">
        <img src="{{asset('/images/logo.png')}}">
    </div>
    <ul >
        <li>
            <a href="{{route('speciesIndex')}}">Administration</a>
        </li>
    </ul>
</nav>
<div class="header">
    <div class="titles">
        <h1>Bienvenue au zoo</h1>
        <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>
            Nullam bibendum vulputate diam, eget ultrices<br>
            dui convallis ac. Mauris gravida faucibus tortor sit amet ultrices.<br>
            Nullam eleifend fermentum velit, nec accumsan enim. </p>
    </div>
</div>
<div class="map">
    <img src="{{asset('/images/img_5.png')}}">
</div>
<div class="presentations">
    <div class="presentation">
        <div class="text">
            <h2>LOREM IPSUM</h2>
            <p>Nunc pulvinar neque non sem rhoncus, quis gravida odio laoreet. Aliquam vitae ante ullamcorper, iaculis leo eget, venenatis turpis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Vivamus tempor metus at orci consectetur dignissim. In posuere sapien nec dui mollis, quis dictum nisl consequat. Sed fringilla finibus mauris ut consectetur. Integer et egestas massa. </p>
        </div>
        <div class="image">
            <img src="{{asset('/images/iguane.png')}}">
        </div>
    </div>
    <div class="presentation">
        <div class="text">
            <h2>LOREM IPSUM</h2>
            <p>Nunc pulvinar neque non sem rhoncus, quis gravida odio laoreet. Aliquam vitae ante ullamcorper, iaculis leo eget, venenatis turpis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Vivamus tempor metus at orci consectetur dignissim. In posuere sapien nec dui mollis, quis dictum nisl consequat. Sed fringilla finibus mauris ut consectetur. Integer et egestas massa. </p>
        </div>
        <div class="image">
            <img src="{{asset('/images/baleine.png')}}">
        </div>
    </div>
    <div class="presentation">
        <div class="text">
            <h2>LOREM IPSUM</h2>
            <p>Nunc pulvinar neque non sem rhoncus, quis gravida odio laoreet. Aliquam vitae ante ullamcorper, iaculis leo eget, venenatis turpis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Vivamus tempor metus at orci consectetur dignissim. In posuere sapien nec dui mollis, quis dictum nisl consequat. Sed fringilla finibus mauris ut consectetur. Integer et egestas massa. </p>
        </div>
        <div class="image">
            <img src="{{asset('/images/bird.png')}}">
        </div>
    </div>
    <div class="presentation">
        <div class="text">
            <h2>LOREM IPSUM</h2>
            <p>Nunc pulvinar neque non sem rhoncus, quis gravida odio laoreet. Aliquam vitae ante ullamcorper, iaculis leo eget, venenatis turpis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Vivamus tempor metus at orci consectetur dignissim. In posuere sapien nec dui mollis, quis dictum nisl consequat. Sed fringilla finibus mauris ut consectetur. Integer et egestas massa. </p>
        </div>
        <div class="image">
            <img src="{{asset('/images/chauve.png')}}">
        </div>
    </div>
    <div class="presentation">
        <div class="text">
            <h2>LOREM IPSUM</h2>
            <p>Nunc pulvinar neque non sem rhoncus, quis gravida odio laoreet. Aliquam vitae ante ullamcorper, iaculis leo eget, venenatis turpis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Vivamus tempor metus at orci consectetur dignissim. In posuere sapien nec dui mollis, quis dictum nisl consequat. Sed fringilla finibus mauris ut consectetur. Integer et egestas massa. </p>
        </div>
        <div class="image">
            <img src="{{asset('/images/scorpion.png')}}">
        </div>
    </div>
</div>
<footer>
    <img src="{{asset('/images/logo.png')}}">
</footer>
