@include('admin.navigation')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>Modification d'une espèce</h1>
            <form method="post" action="{{route('speciesUpdate', ['type'=> $type->id])}}">
                @csrf
                <input type="hidden" name="_method" value="put">
                    <label for="title">Titre</label>
                    <input type="text" value="{{$type->title}}" name="title" required="required">
                    <label for="description">Description</label>
                    <textarea id="description" name="description">{{$type->description}}</textarea>
                <button type="submit" class="button">Enregistrer</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>
