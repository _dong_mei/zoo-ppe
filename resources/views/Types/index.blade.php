@include('admin.navigation')
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="flex-row">
                <h1>Liste des espèces</h1>
                <a class="button" href="{{ route('speciesCreate') }}">Ajouter une espèce</a>
            </div>
            <table class="table" style="width: 100%">
                <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Nom</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($types as $type)
                    <tr>
                        <td>{{$type->id}}</td>
                        <td>{{$type->title}}</td>
                        <td>
                            <a href="{{route('speciesShow', $parameters = ['id'=>$type->id])}}" class='btn btn-info'>Voir</a>
                            <a href="{{route('speciesEdit', $parameters = ['type'=>$type])}}" class='btn btn-info'>Modifier</a>
                            <form method="POST" action="/admin/species/{{$type->id}}/destroy" >
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="submit" value="supprimer"/>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
</div>
</body>
</html>
