@include('admin.navigation')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>Création d'une espèce</h1>
            <form action="" method="POST">
                @csrf
                <label for="title">Titre de la caractéristique</label>
                <input type="text" name="title" required="required">
                <label for="description">Description</label>
                <textarea id="description" name="description"></textarea>
                <input type="submit" class="button" value="Enregistrer">
            </form>
        </div>
    </div>
</div>
<style>

</style>
</body>
</html>
