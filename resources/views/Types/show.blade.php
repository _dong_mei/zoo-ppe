@include('admin.navigation')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>Détail de l'espèce</h1>
                <p class="col-6">Titre: {{$types->title}}</p>
                <p>Description: {{$types->description}}</p>
            <a href="{{route('speciesEdit', $parameters = ['type'=>$types])}}" class='btn btn-info'>Modifier</a>
            <a href="{{route('speciesIndex')}}" class='btn btn-info'>Liste des espèces </a>
        </div>
    </div>
</div>
</body>
</html>
