<?php

namespace Tests\Feature;

use App\Models\Animal;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AnimalTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_index()
    {
        $response = $this->get('/admin/animals');

        $response->assertStatus(200);
    }
    public function test_create_form()
    {
        $response = $this->get('/admin/animals/create');

        $response->assertStatus(200);
    }
    public function test_create_and_delete(){
        $animal = Animal::factory()->create();
        $animal->delete('/admin/animals/'.$animal->id.'/destroy');
        $this->assertDatabaseMissing('animals',['id'=> $animal->id]);
        $this->assertTrue(true, 'animal absente de la base de données');
    }
}
