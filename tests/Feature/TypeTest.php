<?php

namespace Tests\Feature;

use App\Models\Type;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TypeTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_index()
    {
        $response = $this->get('/admin/species');

        $response->assertStatus(200);
    }
    public function test_create_form()
    {
        $response = $this->get('/admin/species/create');
        $response->assertStatus(200);
    }
    public function test_create_and_delete(){
        $type = Type::factory()->create();
        $this->assertTrue(true, 'espèce créée avec succès');
        $type->delete('/admin/species/'.$type->id.'/destroy');
        $this->assertTrue(true, 'espèce suprimée avec succès');
        $this->assertDatabaseMissing('types',['id'=> $type->id]);
        $this->assertTrue(true, 'espèce absente de la base de données');
    }
}
