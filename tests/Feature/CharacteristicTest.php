<?php

namespace Tests\Feature;

use App\Models\Characteristic;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CharacteristicTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_index()
    {
        $response = $this->get('/admin/characteristics');

        $response->assertStatus(200);
    }
    public function test_create_form()
    {
        $response = $this->get('/admin/characteristics/create');

        $response->assertStatus(200);
    }
    public function test_create_and_delete(){
        $char = Characteristic::factory()->create();
        $char->delete('/admin/characteristics/'.$char->id.'/destroy');
        $this->assertDatabaseMissing('characteristics',['id'=> $char->id]);
        $this->assertTrue(true, 'caractéristique absente de la base de données');
    }
}
