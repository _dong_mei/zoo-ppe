<?php

use App\Http\Controllers\AnimalController;
use App\Http\Controllers\CharacteristicController;
use App\Http\Controllers\HomepageController;
use App\Http\Controllers\TypeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',[HomepageController::class,'index'])->name('homepageIndex');
/* Characteristics Routes */
Route::get('/admin/characteristics',[CharacteristicController::class,'index'])->name('characteristicsIndex');
Route::get('/admin/characteristics/create',[CharacteristicController::class,'create'])->name('characteristicsCreate');
Route::post('/admin/characteristics/create',[CharacteristicController::class,'save']);
Route::get('/admin/characteristics/{id}',[CharacteristicController::class,'show'])->name('characteristicsShow');
Route::delete('/admin/characteristics/{id}/destroy', [CharacteristicController::class, 'destroy'])->name('characteristicsDestroy');
Route::put('/admin/characteristics/{characteristic}/edit',[CharacteristicController::class, "update"])->name("characteristicsUpdate");
Route::get('/admin/characteristics/{characteristic}/edit',[CharacteristicController::class, "edit"])->name("characteristicsEdit");
/* Animals Routes */
Route::get('/admin/animals',[AnimalController::class,'index'])->name('animalsIndex');
Route::get('/admin/animals/create',[AnimalController::class,'create'])->name('animalsCreate');
Route::post('/admin/animals/create',[AnimalController::class,'save']);
Route::get('/admin/animals/{id}',[AnimalController::class,'show'])->name('animalsShow');
Route::put('/admin/animals/{animal}/edit',[AnimalController::class, "update"])->name("animalsUpdate");
Route::get('/admin/animals/{animal}/edit',[AnimalController::class, "edit"])->name("animalsEdit");
Route::delete('/admin/animals/{id}/destroy', [AnimalController::class, 'destroy'])->name('animalsDestroy');
/* Species Routes */
Route::get('/admin/species',[TypeController::class,'index'])->name('speciesIndex');
Route::get('/admin/species/create',[TypeController::class,'create'])->name('speciesCreate');
Route::post('/admin/species/create',[TypeController::class,'save']);
Route::get('/admin/species/{id}',[TypeController::class,'show'])->name('speciesShow');
Route::put('/admin/species/{type}/edit',[TypeController::class, "update"])->name("speciesUpdate");
Route::get('/admin/species/{type}/edit',[TypeController::class, "edit"])->name("speciesEdit");
Route::delete('/admin/species/{id}/destroy', [TypeController::class, 'destroy'])->name('speciesDestroy');
