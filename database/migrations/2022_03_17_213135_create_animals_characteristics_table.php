<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animals_characteristics', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('animal_id')->unsigned()->nullable(true);
            $table->foreign('animal_id')->references('id')->on('animals')->onDelete('set null');
            $table->unsignedBigInteger('characteristic_id')->unsigned()->nullable(true);
            $table->foreign('characteristic_id')->references('id')->on('characteristics')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animals_characteristics');
    }
};
