<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('animals', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable(false);
            $table->text('gestation')->nullable(true);
            $table->text('weight')->nullable(true);
            $table->text('heigth')->nullable(true);
            $table->text('environment')->nullable(true);
            $table->text('diet')->nullable(true);
            $table->text('lifetime')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('animals');
    }
};
