<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Characteristic;
use App\Models\Animal;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Characteristic::factory(200)->create();
        \App\Models\Type::factory(20)->create();
        \App\Models\Animal::factory(1000)->create();

        // Get all the characteristics attaching up to 3 random characteristics to each animal
        $characteristics = Characteristic::all();

        // Populate the pivot table
        Animal::all()->each(function ($animal) use ($characteristics) {
            $animal->characteristics()->attach(
                $characteristics->random(rand(5, 15))->pluck('id')->toArray()
            );
        }
        );
    }
}

