<?php

namespace Database\Factories;

use App\Models\Type;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Animal>
 */
class AnimalFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name'=>$this->faker->word(),
            'gestation'=>$this->faker->sentence(),
            'weight'=>$this->faker->sentence(),
            'heigth'=>$this->faker->sentence(),
            'environment'=>$this->faker->sentence(),
            'diet'=>$this->faker->text(),
            'lifetime'=>$this->faker->sentence(),
            'type_id' => Type::pluck('id')[$this->faker->numberBetween(1,Type::count()-1)]
        ];
    }
}
