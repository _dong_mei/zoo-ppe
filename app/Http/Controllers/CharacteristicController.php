<?php

namespace App\Http\Controllers;

use App\Models\Characteristic;
use Illuminate\Http\Request;

class CharacteristicController extends Controller
{
    public function index()
    {
        $characteristics = Characteristic::all();
        return view('Characteristics/index', ['characteristics' => $characteristics ]);
    }
    public function show($id){
        $characteristics = Characteristic::findOrFail($id);
        return view('Characteristics/show',['characteristics' => $characteristics]);
    }
    public function create(){
        return view('Characteristics/create');
    }
    public function save(Request $request){
        $request->validate([
            'title' => 'required',
        ]);
        Characteristic::create($request->all());
        return redirect()->route('characteristicsIndex')->with('success', 'Caractéristique ajoutée avec succès');
    }
    public function destroy($id)
    {
        $characteristic = Characteristic::findOrFail($id);
        $characteristic->delete();
        return redirect()->route('characteristicsIndex')->with('success', 'Caractéristique supprimée avec succès');
    }

    public function update(Request $request, Characteristic $characteristic)
    {
        $request->validate([
            'title' => 'required',
        ]);
        $characteristic->update([
            'title' => $request->get('title')
        ]);
        return redirect()->route('characteristicsIndex')->with('success', 'Caractéristique mise à jour avec succès');
    }

    public function edit(Characteristic $characteristic)
    {
        return view("Characteristics/edit", compact('characteristic'));
    }



}
