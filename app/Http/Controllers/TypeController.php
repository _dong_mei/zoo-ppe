<?php

namespace App\Http\Controllers;

use App\Models\Type;
use Illuminate\Http\Request;

class TypeController extends Controller
{
    public function index()
    {
        $types = Type::all();
        return view('Types/index', ['types' => $types ]);
    }
    public function show($id){
        $types = Type::findOrFail($id);
        return view('Types/show',['types' => $types]);
    }
    public function create(){
        return view('Types/create');
    }
    public function save(Request $request){
        Type::create($request->all());
        return redirect()->route('speciesIndex')->with('success', 'Espèce ajoutée avec succès');
    }
    public function destroy($id)
    {
        $type = Type::findOrFail($id);
        $animals = $type->animals;
        if(count($animals)>0) {
            foreach ($animals as $animal) {
                $animal->type_id = 28;
                $animal->save();
            }
        }
        $type->delete();
        return redirect()->route('speciesIndex')->with('success', 'Espèce supprimée avec succès');
    }

    public function update(Request $request, Type $type)
    {

        $type->update([
            'title' => $request->get('title'),
            'description'=>$request->get('description')
        ]);
        return redirect()->route('speciesIndex')->with('success', 'Espèce mise à jour avec succès');
    }

    public function edit(Type $type)
    {
        return view("Types/edit", compact('type'));
    }
}
