<?php

namespace App\Http\Controllers;

use App\Models\Animal;
use App\Models\Characteristic;
use App\Models\Type;
use Illuminate\Http\Request;

class AnimalController extends Controller
{
    public function index()
    {
        $animals = Animal::all();
        return view('Animals/index', ['animals' => $animals ]);
    }
    public function show($id){
        $animals = Animal::findOrFail($id);
        $type = Type::findOrFail($animals->type_id);
        return view('Animals/show',['animals' => $animals, 'type'=>$type]);
    }
    public function create(){
        $characteristics = Characteristic::all();
        $types = Type::all();
        return view('Animals/create', ['types'=>$types, 'characteristics'=>$characteristics]);
    }
    public function save(Request $request){
        $animal = Animal::create($request->all());
        $animal->characteristics()->sync($request->get('characteristics'), false);

        return redirect()->route('animalsIndex')->with('success', 'Animal ajouté avec succès');
    }
    public function update(Request $request, Animal $animal)
    {

        $animal->name = $request->get('name');
        $animal->gestation = $request->get('gestation');
        $animal->weight =  $request->get('weight');
        $animal->heigth = $request->get('height');
        $animal->environment = $request->get('environment');
        $animal->diet = $request->get('diet');
        $animal->lifetime = $request->get('lifetime');
        $animal->type_id = $request->get('type_id');
        $animal->save();
        $animal->characteristics()->sync($request->get('characteristics'), true);

        return redirect()->route('animalsIndex')->with('success', 'Animal mis à jour avec succès');
    }

    public function edit(Animal $animal)
    {
        $characteristics = Characteristic::all();
        $types = Type::all();
        return view("Animals/edit", compact('animal', 'characteristics', 'types'));
    }
    public function destroy($id)
    {
        $animal = Animal::findOrFail($id);
        $animal->delete();
        return redirect('/admin/animals')->with('success', 'Animal supprimé avec succès');
    }
}
