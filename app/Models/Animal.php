<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Animal extends Model
{
    use HasFactory;
    public function characteristics(){
        return $this->BelongsToMany(Characteristic::CLASS,'animals_characteristics',
            'animal_id', 'characteristic_id');
    }
    public function types(){
        return $this->HasMany(Type::CLASS);
    }
    protected $guarded = ['id'];
    protected $fillable = ['name', 'gestation', 'weight', 'heigth', 'environment', 'diet', 'lifetime', 'type_id'];
}
